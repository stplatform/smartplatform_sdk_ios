# Tamra

[![CI Status](http://img.shields.io/travis/joel.ryu/Tamra.svg?style=flat)](https://travis-ci.org/joel.ryu/Tamra)
[![Version](https://img.shields.io/cocoapods/v/Tamra.svg?style=flat)](http://cocoapods.org/pods/Tamra)
[![License](https://img.shields.io/cocoapods/l/Tamra.svg?style=flat)](http://cocoapods.org/pods/Tamra)
[![Platform](https://img.shields.io/cocoapods/p/Tamra.svg?style=flat)](http://cocoapods.org/pods/Tamra)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

Tamra is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "Tamra"
```

## Author

joel.ryu, joel.ryu@daumkakao.com

## License

Tamra is available under the MIT license. See the LICENSE file for more info.
