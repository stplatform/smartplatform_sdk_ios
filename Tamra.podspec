Pod::Spec.new do |s|
  s.name             = 'Tamra'
  s.version          = '0.1.0'
  s.summary          = 'iOS Beacon SDK for Smart Tourism Platform'

  s.description      = <<-DESC
Smart Tourism Platform Homepage : http://jstp.jejutour.go.kr
Smart Tourism Platform Documents : http://jstp.jejutour.go.kr/#/documents
                       DESC

  s.homepage         = 'http://jstp.jejutour.go.kr'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'cocoon.tf' => 'cocoon.tf@kakaocorp.com' }
  s.source           = { :git => 'https://bitbucket.org/stplatform/smartplatform_sdk_ios', :tag => s.version.to_s }

  s.ios.deployment_target = '8.3'

  s.source_files = 'Tamra/Classes/**/*'
  
  s.dependency 'ReachabilitySwift', '2.3'
  s.dependency 'Alamofire', '3.5.0'
  s.dependency 'RealmSwift', '1.1.0'
  s.dependency 'CryptoSwift', '0.5.2'
end
