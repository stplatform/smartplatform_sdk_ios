import UIKit

open class Tamra {

    fileprivate static let deviceChecker = DeviceChecker.sharedInstance
    fileprivate static let historyManager: HistoryManager = BasicHistoryManager()
    fileprivate static var config: TamraConfig?
    fileprivate static let platformId = "ios"

    static var _status: TamraStatus = .not_INITIALIZED
    open static var status: TamraStatus {
        return _status
    }

    open static var locationStatus: TamraLocationStatus {
        return deviceChecker.location
    }

    open static var bluetoothStatus: TamraBluetoothStatus {
        return deviceChecker.bluetooth
    }

    open static var internetStatus: TamraInternetStatus {
        return deviceChecker.internet
    }

    static var profile: TamraProfile? {
        return config?.profile
    }

    static var appId: String? {
        return config?.appId
    }

    static var appKey: String? {
        return config?.appKey
    }

    static var automaticMonitoring: Bool {
        if let value = config?.automaticMonitoring {
            return value
        }

        return false
    }

    static var usingSimulation: Bool {
        if let value = config?.simulation {
            return value
        }

        return false
    }

    static var deviceName: String? {
        return config?.deviceName
    }

    static var sensitivity: TamraSensitivity {
        if let sensitivity = config?.sensitivity {
            return sensitivity
        }

        return .balanced
    }

    static var uuid: String? {
        return config?.uuid.uuidString.lowercased()
    }

    static var cid: String {
        return "\(platformId)/\(appId!)/\(uuid!)"
    }

    open static func configure(_ config: TamraConfig) {
        self.config = config
        Logger.level = config.logLevel
        _status = .initialized
    }

    open static func requestLocationAuthorization() {
        deviceChecker.requestLocationAuthorization()
    }

    open static func histories(_ spot: TamraSpot) -> TamraHistories {
        return historyManager.hasVisited(spot)
    }

    open static func histories(_ n: Int) -> TamraHistories {
        return historyManager.last(n)
    }

    open static func histories(_ period: TamraHistoryPeriod) -> TamraHistories {
        return historyManager.fetch(period)
    }

    open static func logHistory(_ spot: TamraSpot) {
        historyManager.log(spot)
    }

    open static func clearHistory() {
        historyManager.clear()
    }

    open static func recentErrors() -> [NSError] {
        // TODO 현재는 그냥 빈 값만 넘김.. Swift 에러처리 정책이 정해지면 구현
        return []
    }

}

open class TamraConfig {

    var appId: String {
        return Bundle.main.bundleIdentifier!
    }

    var uuid: UUID {
        return UIDevice.current.identifierForVendor!
    }

    let appKey: String
    let profile: TamraProfile
    let sensitivity: TamraSensitivity
    let simulation: Bool
    let automaticMonitoring: Bool
    let deviceName: String
    let logLevel: TamraLoggerLevel

    public init(appKey: String,
                profile: TamraProfile,
                sensitivity: TamraSensitivity = .balanced,
                deviceName: String = UIDevice.current.name,
                simulation: Bool = false,
                automaticMonitoring: Bool = false,
                logLevel: TamraLoggerLevel = .warn) {
        self.appKey = appKey
        self.profile = profile
        self.sensitivity = sensitivity
        self.deviceName = deviceName
        self.simulation = simulation
        self.automaticMonitoring = automaticMonitoring
        self.logLevel = logLevel
    }

    open static func forRelease(_ appKey: String) -> TamraConfig {
        return TamraConfig(appKey: appKey,
                           profile: .production)
    }

    open static func forTest(_ appKey: String) -> TamraConfig {
        return TamraConfig(appKey: appKey,
                           profile: .test)
    }

    open func sensitivity(_ sensitivity: TamraSensitivity) -> TamraConfig {
        return TamraConfig(appKey: appKey,
                           profile: profile,
                           sensitivity: sensitivity,
                           deviceName: deviceName,
                           simulation: simulation,
                           automaticMonitoring: automaticMonitoring,
                           logLevel: logLevel)
    }

    open func onSimulation(_ deviceName: String = UIDevice.current.name) -> TamraConfig {
        return TamraConfig(appKey: appKey,
                           profile: profile,
                           sensitivity: sensitivity,
                           deviceName: deviceName,
                           simulation: true,
                           automaticMonitoring: automaticMonitoring,
                           logLevel: logLevel)
    }

    open func onAutomaticMonitoring() -> TamraConfig {
        return TamraConfig(appKey: appKey,
                           profile: profile,
                           sensitivity: sensitivity,
                           deviceName: deviceName,
                           simulation: simulation,
                           automaticMonitoring: true,
                           logLevel: logLevel)
    }

}

public enum TamraProfile {
    case develop
    case test
    case production
}

public enum TamraStatus {
    case not_INITIALIZED
    case initialized
    case init_FAILED
}

public enum TamraLoggerLevel: Int {
    case debug = 1
    case info
    case warn
    case error
}

public enum TamraLocationStatus {
    case available
    case unavailable
    case unknown
}

public enum TamraBluetoothStatus {
    case on
    case off
    case unavailable
    case unknown
}

public enum TamraInternetStatus {
    case wiFi
    case cellular
    case notReachable
    case unknown
}

public enum TamraSensitivity {
    case balanced
    case responsive
    case accurate
}
