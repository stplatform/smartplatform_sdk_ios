import Foundation
import CoreLocation

class RangedBeacon: NSObject {

    let region: Region
    let major: Int
    let minor: Int
    let rangedAt: Date

    init(region: Region, beacon: Beacon, rangedAt: Date) {
        self.region = region
        self.major = beacon.major
        self.minor = beacon.minor
        self._accuracy = beacon.accuracy
        self._proximity = beacon.proximity.stringValue
        self.rangedAt = rangedAt

        super.init()
    }

    init(region: Region, major: Int, minor: Int, accuracy: Double, rangedAt: Date) {
        self.region = region
        self.major = major
        self.minor = minor
        self._accuracy = accuracy
        switch accuracy {
        case 0.0 ... 1.0:
            self._proximity = Proximity.immediate.stringValue
        case 1.0 ... 3.0:
            self._proximity = Proximity.near.stringValue
        case 3.0 ... 10.0:
            self._proximity = Proximity.far.stringValue
        default:
            self._proximity = Proximity.unknown.stringValue
        }
        self.rangedAt = rangedAt

        super.init()
    }

    fileprivate init(rangedBeacon: RangedBeacon) {
        self.region = rangedBeacon.region
        self.major = rangedBeacon.major
        self.minor = rangedBeacon.minor
        self._accuracy = rangedBeacon.accuracy
        self._proximity = rangedBeacon.proximity
        self.rangedAt = rangedBeacon.rangedAt

        super.init()
    }

    let _accuracy: Double
    var accuracy: Double {
        return _accuracy
    }

    let _proximity: String
    var proximity: String {
        return _proximity
    }

    var beaconKey: BeaconKey {
        return BeaconKey(uuid: region.uuid, major: major, minor: minor)
    }

    func clone() -> RangedBeacon {
        return RangedBeacon(rangedBeacon: self)
    }
}
