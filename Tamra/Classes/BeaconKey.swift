import Foundation

struct BeaconKey {

    let uuid: UUID
    let major: Int
    let minor: Int

    init(uuid: UUID, major: Int, minor: Int) {
        self.uuid = uuid
        self.major = major
        self.minor = minor
    }

    init(beacon: Beacon) {
        self.uuid = beacon.uuid as UUID
        self.major = beacon.major
        self.minor = beacon.minor
    }

}

extension BeaconKey: Hashable {

    var hashValue: Int {
        var result = uuid.hashValue
        result = 31 * result + major
        result = 31 * result + minor
        return result
    }

}

extension BeaconKey: Equatable {

}

func ==(lhs: BeaconKey, rhs: BeaconKey) -> Bool {
    return lhs.uuid == rhs.uuid && lhs.major == rhs.major && lhs.minor == rhs.minor
}
