import Foundation
import Alamofire

protocol DataSource {

    func regionDatas(_ done: @escaping ([RegionData]?) -> Void)

    func beaconDatas(_ done: @escaping ([BeaconData]?) -> Void)

    func spot(byBeaconId id: Int, done: @escaping (String?) -> Void)

    func simulatedSignals(_ deviceName: String, done: @escaping ([SimulatedSignal]?) -> Void)

}

class NetworkDataSource: DataSource, APISupport {

    static let sharedInstance = NetworkDataSource()

    fileprivate let deviceChecker = DeviceChecker.sharedInstance
    fileprivate let logger = Logger(name: String(describing: NetworkDataSource.self))

    fileprivate var _storedToken: Token? = nil
    var storedToken: Token? {
        return _storedToken
    }

    fileprivate init() {

    }

    func storeToken(_ token: Token?) {
        _storedToken = token
    }

    func regionDatas(_ done: @escaping ([RegionData]?) -> Void) {
        if !deviceChecker.internetConnectable {
            done(nil)
            return
        }

        withAuthentication {
            authentication in
            //Alamofire.request(.GET, self.apiURL("/regions"), parameters: ["limit": 1000], headers: authentication.headers)
            Alamofire.request(self.apiURL("/regions"), method: .get, parameters: ["limit": 1000], encoding: JSONEncoding.default, headers: authentication.headers)
            .responseJSON {
                response in
                switch response.result {
                case .success(let data):
                    let array = data as! Array<Dictionary<String, AnyObject>>
                    let regionDatas = array.map {
                        item in
                        return RegionData(dictionary: item)
                    }

                    done(regionDatas)
                case .failure(let error):
                    self.logger.error("Fetching regions is failed.\n\(error)")
                    done(nil)
                }
            }
        }
    }

    func beaconDatas(_ done: @escaping ([BeaconData]?) -> Void) {
        if !deviceChecker.internetConnectable {
            done(nil)
            return
        }

        withAuthentication {
            authentication in
            //Alamofire.request(.GET, self.apiURL("/beacons"), parameters: ["limit": 1000], headers: authentication.headers)
            Alamofire.request(self.apiURL("/beacons"), method: .get, parameters: ["limit": 1000], headers: authentication.headers)
            .responseJSON {
                response in
                switch response.result {
                case .success(let data):
                    let array = data as! Array<Dictionary<String, AnyObject>>
                    let beaconDatas = array.map {
                        item in
                        return BeaconData(dictionary: item)
                    }

                    done(beaconDatas)
                case .failure(let error):
                    self.logger.error("Fetching beacons is failed.\n\(error)")
                    done(nil)
                }
            }
        }
    }

    func spot(byBeaconId id: Int, done: @escaping (String?) -> Void) {
        if !deviceChecker.internetConnectable {
            done(nil)
            return
        }

        withAuthentication {
            authentication in
            //Alamofire.request(.GET, self.apiURL("/spots/\(id)"), headers: authentication.headers)
            Alamofire.request(self.apiURL("/spots/\(id)"), method: .get, headers: authentication.headers)
            .responseString {
                response in
                switch response.result {
                case .success(let data):
                    done(data)
                case .failure(let error):
                    self.logger.error("Fetching spot is failed.\n\(error)")
                    done(nil)
                }
            }
        }
    }

    func simulatedSignals(_ deviceName: String, done: @escaping ([SimulatedSignal]?) -> Void) {
        if !deviceChecker.internetConnectable {
            done(nil)
            return
        }

        withAuthentication {
            authentication in
            /*Alamofire.request(.GET,
                              self.apiURL("/locationsimulation/signals"),
                              parameters: ["deviceName": deviceName],
                              headers: authentication.headers)*/
            Alamofire.request(self.apiURL("/locationsimulation/signals"), method: .get, parameters: ["deviceName": deviceName], headers: authentication.headers)
            .responseJSON {
                response in
                switch response.result {
                case .success(let data):
                    let array = data as! [[String:AnyObject]]
                    let simulatedSignals = array.map {
                        item in
                        return SimulatedSignal(dictionary: item)
                    }
                    done(simulatedSignals)
                case .failure(let error):
                    self.logger.error("Fetching simulated signals is failed.\n\(error)")
                    done(nil)
                }
            }
        }
    }

}
