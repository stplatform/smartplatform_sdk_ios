import Foundation

extension Date {

    func utcString() -> String {
        return DateFormatters.sharedInstance.utc.string(from: self)
    }

}

extension String {

    func utcDate() -> Date? {
        return DateFormatters.sharedInstance.utc.date(from: self)
    }

}


class DateFormatters {

    static let sharedInstance = DateFormatters()

    let utc: DateFormatter = DateFormatter()

    fileprivate init() {
        utc.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    }

}
