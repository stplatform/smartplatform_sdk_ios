import Foundation

@objc
open class TamraRegion: NSObject {

    fileprivate static let logger = Logger(name: String(describing: TamraRegion.self))

    fileprivate let regionData: RegionData?

    open var id: Int? {
        return regionData?.id
    }

    open var name: String? {
        return regionData?.name
    }

    var isMeta: Bool {
        if let regionType = regionData?.regionType {
            if regionType == "Meta" {
                return true
            }
        }

        return false
    }

    var uuid: UUID? {
        return regionData?.uuid as! UUID
    }

    init(regionData: RegionData?) {
        self.regionData = regionData
    }

}
