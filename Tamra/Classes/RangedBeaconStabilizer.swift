import Foundation
import CoreLocation

protocol RangedBeaconStabilizer {

    func ranged(_ rangedBeacons: [RangedBeacon])

    func stabledBeacons() -> [RangedBeacon]

}

class TTLAvgRangedBeaconStabilizer: RangedBeaconStabilizer {

    fileprivate static let DEFAULT_TTL_SEC: Double = 5.0
    fileprivate static let DEFAULT_MIN_RANGE_COUNT: Int = 3
    fileprivate let logger = Logger(name: String(describing: TTLAvgRangedBeaconStabilizer.self))

    fileprivate let ttlSecond: Double
    fileprivate let minRangeCount: Int
    fileprivate var rangedBeacons: [RangedBeacon] = []
    fileprivate let lockQueue = DispatchQueue(label: "\(String(describing: TTLAvgRangedBeaconStabilizer.self))-LockQueue", attributes: [])

    init() {
        self.ttlSecond = TTLAvgRangedBeaconStabilizer.DEFAULT_TTL_SEC
        self.minRangeCount = TTLAvgRangedBeaconStabilizer.DEFAULT_MIN_RANGE_COUNT
    }

    init(ttlSecond: Double, minRangeCount: Int) {
        self.ttlSecond = ttlSecond
        self.minRangeCount = minRangeCount
    }

    func ranged(_ rangedBeacons: [RangedBeacon]) {
        lockQueue.sync {
            self.rangedBeacons += rangedBeacons.filter {
                $0.accuracy > 0
            }
        }
    }

    fileprivate func removeExpired() {
        let baseTime = Date().addingTimeInterval(-self.ttlSecond)

        lockQueue.sync {
            self.rangedBeacons = self.rangedBeacons.filter {
                beacon in
                return baseTime.compare(beacon.rangedAt as Date) == .orderedAscending
            }
        }
    }

    func stabledBeacons() -> [RangedBeacon] {
        removeExpired()

        var dict: [BeaconKey:[RangedBeacon]] = [:]

        lockQueue.sync {
            dict = self.rangedBeacons.reduce([BeaconKey: [RangedBeacon]]()) {
                (dict, rangedBeacon) in
                var dict = dict
                let beaconKey = rangedBeacon.beaconKey
                if let array = dict[beaconKey] {
                    dict[beaconKey] = array + [rangedBeacon]
                } else {
                    dict[beaconKey] = [rangedBeacon]
                }

                return dict
            }
        }

        return Array(dict.values).filter({ $0.count >= minRangeCount }).map {
            rangedBeacons in
            let baseRangedBeacon = rangedBeacons.first!

            let avgAccuracy = rangedBeacons.reduce(0.0) {
                (sum, rangedBeacon) in
                return sum + rangedBeacon.accuracy
            } / Double(rangedBeacons.count)

            return RangedBeacon(region: baseRangedBeacon.region,
                                major: baseRangedBeacon.major,
                                minor: baseRangedBeacon.minor,
                                accuracy: avgAccuracy,
                                rangedAt: baseRangedBeacon.rangedAt)
        }
    }

}
