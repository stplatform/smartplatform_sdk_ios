import Foundation
import CoreLocation

extension CLBeaconRegion {

    convenience init?(for tamraRegion: TamraRegion) {
        guard let uuid = tamraRegion.uuid else {
            return nil
        }

        guard let name = tamraRegion.name else {
            return nil
        }

        self.init(proximityUUID: uuid as UUID, identifier: name)
        notifyOnEntry = true
        notifyOnExit = true
        notifyEntryStateOnDisplay = true
    }

}

extension CLProximity {

    var stringValue: String {
        switch self {
        case .unknown:
            return "Unknown"
        case .immediate:
            return "Immediate"
        case .near:
            return "Near"
        case .far:
            return "Far"
        }
    }

}
