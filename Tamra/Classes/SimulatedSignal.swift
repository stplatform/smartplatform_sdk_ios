import Foundation
import CryptoSwift

struct SimulatedSignal {

    fileprivate static let key: Array<UInt8> = "XN4lyku82GbSn5UJ".utf8.map({ $0 })

    let id: Int
    let accuracy: Double

    fileprivate let beaconSpec: BeaconSpec

    var uuid: UUID {
        return beaconSpec.uuid
    }

    var major: Int {
        return beaconSpec.major
    }

    var minor: Int {
        return beaconSpec.minor
    }

    init(dictionary: [String:AnyObject]) {
        id = dictionary["beaconId"] as! Int
        accuracy = dictionary["accuracy"] as! Double
        beaconSpec = BeaconSpec(encryptedString: dictionary["support"] as! String)
    }

    fileprivate init(signal: SimulatedSignal) {
        id = signal.id
        accuracy = signal.accuracy
        beaconSpec = signal.beaconSpec
    }

    func clone() -> SimulatedSignal {
        return SimulatedSignal(signal: self)
    }

    struct BeaconSpec {

        let uuid: UUID
        let major: Int
        let minor: Int
        let txPower: Int

        init(encryptedString: String) {
            //let cipher = try! AES(key: key, blockMode: .ecb)
            let cipher = try! AES(key: key, blockMode: .ECB)
            let decrypted = try! encryptedString.decryptBase64(cipher: cipher)
            let dict = try! JSONSerialization.jsonObject(with: Data(bytes: decrypted), options: JSONSerialization.ReadingOptions()) as! [String:AnyObject]

            uuid = UUID(uuidString: dict["uuid"] as! String)!
            major = dict["major"] as! Int
            minor = dict["minor"] as! Int
            txPower = dict["txPower"] as! Int
        }

    }

}
