import Foundation

class Logger {

    fileprivate static var minimumLevel: TamraLoggerLevel = .warn
    static var level: TamraLoggerLevel {
        get {
            return minimumLevel
        }

        set(newValue) {
            minimumLevel = newValue
        }
    }

    fileprivate var dateFormatter = DateFormatter()
    fileprivate var name: String
    fileprivate let syncQueue = DispatchQueue(label: "\(String(describing: Logger.self))-SyncQueue", attributes: [])

    init(name: String) {
        self.name = name
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
    }

    fileprivate func log(_ format: String, level: TamraLoggerLevel = .debug, arguments: [CVarArg]) {
        if level.rawValue < Logger.minimumLevel.rawValue {
            return
        }

        let levelString: String

        switch level {
        case .debug:
            levelString = "DEBUG"
        case .info:
            levelString = "INFO"
        case .warn:
            levelString = "WARN"
        case .error:
            levelString = "ERROR"
        }

        let timestamp = dateFormatter.string(from: Date())
        let message = String(format: "\(timestamp) [\(levelString)] \(name) - " + format, arguments: arguments)

        syncQueue.sync {
            print(message)
        }
    }

}

extension Logger {

    func debug(_ format: String, _ args: CVarArg...) {
        log(format, level: .debug, arguments: args)
    }

    func info(_ format: String, _ args: CVarArg...) {
        log(format, level: .info, arguments: args)
    }

    func warn(_ format: String, _ args: CVarArg...) {
        log(format, level: .warn, arguments: args)
    }

    func error(_ format: String, _ args: CVarArg...) {
        log(format, level: .error, arguments: args)
    }

}
