import Foundation

class BeaconData: NSObject {

    fileprivate let logger = Logger(name: String(describing: BeaconData.self))

    let id: Int
    let regionId: Int
    let beaconType: String
    let uuid: UUID
    let major: Int
    let minor: Int
    let indoor: Bool
    let movable: Bool
    let txPower: Int
    let latitude: Double
    let longitude: Double
    let geohash: String
    let desc: String

    var beaconKey: BeaconKey {
        return BeaconKey(uuid: uuid, major: major, minor: minor)
    }

    init(id: Int,
         regionId: Int,
         beaconType: String,
         uuid: UUID,
         major: Int,
         minor: Int,
         indoor: Bool,
         movable: Bool,
         txPower: Int,
         latitude: Double,
         longitude: Double,
         geohash: String,
         desc: String) {
        self.id = id
        self.regionId = regionId
        self.beaconType = beaconType
        self.uuid = uuid
        self.major = major
        self.minor = minor
        self.indoor = indoor
        self.movable = movable
        self.txPower = txPower
        self.latitude = latitude
        self.longitude = longitude
        self.geohash = geohash
        self.desc = desc
        super.init()
    }

    init(dictionary: Dictionary<String, AnyObject>) {
        self.id = dictionary["id"] as! Int
        self.regionId = dictionary["regionId"] as! Int
        self.beaconType = dictionary["beaconType"] as! String
        self.uuid = UUID(uuidString: dictionary["uuid"] as! String)!
        self.major = dictionary["major"] as! Int
        self.minor = dictionary["minor"] as! Int
        self.indoor = dictionary["indoor"] as! Bool
        self.movable = dictionary["movable"] as! Bool
        self.txPower = dictionary["txPower"] as! Int
        self.latitude = dictionary["latitude"] as! Double
        self.longitude = dictionary["longitude"] as! Double
        self.geohash = dictionary["geohash"] as! String
        self.desc = dictionary["description"] as! String
        super.init()
    }

}
