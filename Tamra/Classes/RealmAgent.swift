import Foundation
import RealmSwift

class RealmAgent {

    static let sharedInstance = RealmAgent()
    fileprivate var config: Realm.Configuration

    var defaultRealm: Realm {
        return try! Realm(configuration: config)
    }

    fileprivate init() {
        config = Realm.Configuration()
        //config.fileURL = config.fileURL!.deletingLastPathComponent()?.appendingPathComponent("tamra-for-oreum.realm")
        // TODO : optional chaning 의 제거가 미치는 영향을 확인해야 한다.
        config.fileURL = config.fileURL!.deletingLastPathComponent().appendingPathComponent("tamra-for-oreum.realm")
    }

}

class RealmDefaultValue {
    static let int = -1
    static let double = -1.0
    static let string = "_unknown_"
    static let bool = false
}
