import Foundation
import Alamofire

protocol APISupport {

    var storedToken: Token? { get }
    func storeToken(_ token: Token?)

}

extension APISupport {

    func withAuthentication(_ f: @escaping (Authentication) -> Void) {
        if let issuedToken = storedToken {
            f(Authentication(token: issuedToken))
        } else {
            requestToken {
                token in
                Logger(name: String(describing: APISupport.self)).debug("issued token : \(token)")
                self.storeToken(token)
                f(Authentication(token: token!))
            }
        }
    }

    fileprivate func requestToken(_ done: @escaping (Token?) -> Void) {
        let headers: [String: String] = [
                "Oreum-CID": Tamra.cid,
        ]

        let parameters = [
                "appId": Tamra.appId!,
                "appKey": Tamra.appKey!
        ]

        //Alamofire.request(.POST, apiURL("/auth/authenticate"), headers: headers, parameters: parameters, encoding: .JSON)
        Alamofire.request(apiURL("/auth/authenticate"), method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        .validate()
        .responseJSON {
            response in
            switch response.result {
            case .success(let data):
                let json = data as! Dictionary<String, AnyObject>
                let token = json["token"] as! String
                let expireAt = json["expireAt"] as! String
                done(Token(token: token, expireAt: expireAt.utcDate()!))
            case .failure(let error):
                Logger(name: String(describing: APISupport.self)).error("Authentication is failed.\n%@", error as CVarArg)
                done(nil)
            }
        }
    }

    func apiURL(_ path: String) -> String {
        var host: String = "http://localhost"

        if let profile = Tamra.profile {
            switch profile {
            case .develop:
                //host = "http://220.124.222.194"
                host = "http://220.124.222.184"
            case .test:
                //host = "http://220.124.222.194"
                host = "http://220.124.222.184"
            case .production:
                host = "http://jstpsdk.jejutour.go.kr"
            }
        }

        return host + path
    }

}

struct Authentication {

    let token: Token

    var headers: [String:String] {
        return [
                "Oreum-CID": Tamra.cid,
                "Oreum-Token": token.token
        ]
    }

    init(token: Token) {
        self.token = token
    }

}

struct Token {

    let token: String
    let expireAt: Date

    init(token: String, expireAt: Date) {
        self.token = token
        self.expireAt = expireAt
    }

}
