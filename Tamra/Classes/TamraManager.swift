import Foundation

open class TamraManager: NSObject {

    fileprivate let logger = Logger(name: String(describing: TamraManager.self))
    fileprivate let repository = DataRepository.sharedInstance
    fileprivate let oreumBeaconManager = OreumBeaconManager()

    fileprivate var recentlyNearbySpots: TamraNearbySpots?

    public override init() {
        super.init()
        oreumBeaconManager.delegate = self
    }

    fileprivate var _delegate: TamraManagerDelegate?
    open var delegate: TamraManagerDelegate? {
        get {
            return _delegate
        }

        set {
            _delegate = newValue
        }
    }

    open func ready() {
        oreumBeaconManager.automaticMonitoring = Tamra.automaticMonitoring
        oreumBeaconManager.usingSimulation = Tamra.usingSimulation
        oreumBeaconManager.deviceName = Tamra.deviceName
        switch Tamra.sensitivity {
        case .responsive:
            oreumBeaconManager.sensitivity = .responsive
        case .accurate:
            oreumBeaconManager.sensitivity = .accurate
        case .balanced:
            oreumBeaconManager.sensitivity = .balanced
        }
        oreumBeaconManager.start()

        if repository.acceptable {
            self._delegate?.tamraManager?(didReadyState: .ignored)
            return
        }

        repository.reload {
            isLoaded in
            if isLoaded {
                self._delegate?.tamraManager?(didReadyState: .loaded)
            } else {
                self._delegate?.tamraManager?(didReadyState: .loadingFailed)
            }
        }
    }

    open func startMonitoring(forId regionId: Int) {
        oreumBeaconManager.startMonitoring(forId: regionId)
    }

    open func stopMonitoring(forId regionId: Int) {
        oreumBeaconManager.stopMonitoring(forId: regionId)
    }

    open func startMonitoring() {
        oreumBeaconManager.startMonitoring()
    }

    open func stopMonitoring() {
        oreumBeaconManager.stopMonitoring()
    }

    open func spots() -> TamraNearbySpots? {
        return recentlyNearbySpots
    }
}

extension TamraManager: OreumBeaconManagerDelegate {

    func oreumBeaconManager(didEnterRegion regionData: RegionData) {
        let region = TamraRegion(regionData: regionData)
        _delegate?.tamraManager?(didEnterRegion: region)
    }

    func oreumBeaconManager(didExitRegion regionData: RegionData) {
        let region = TamraRegion(regionData: regionData)
        _delegate?.tamraManager?(didExitRegion: region)
    }

    func oreumBeaconManager(didRangeBeacons beacons: [RangedBeacon]) {
        TamraNearbySpots.fetch(beacons) {
            nearbySpots in
            self.recentlyNearbySpots = nearbySpots
            self._delegate?.tamraManager?(didRangeSpots: nearbySpots)
        }
    }

}

@objc
public protocol TamraManagerDelegate: NSObjectProtocol {

    @objc optional func tamraManager(didEnterRegion region: TamraRegion)

    @objc optional func tamraManager(didExitRegion region: TamraRegion)

    @objc optional func tamraManager(didRangeSpots spots: TamraNearbySpots)

    @objc optional func tamraManager(didReadyState state: TamraManagerState)

}

@objc
public enum TamraManagerState: Int {

    case loaded
    case loadingFailed
    case ignored

}
