import Foundation

class OreumBeaconManager: NSObject {

    fileprivate let logger = Logger(name: String(describing: OreumBeaconManager.self))
    fileprivate let repository = DataRepository.sharedInstance
    fileprivate let beaconManager = BeaconManager()
    fileprivate let sdkLogger = SDKLoggers.defaultLogger
    fileprivate let metaUUID = UUID(uuidString: "9f380d57-854c-4148-bbb4-cfaeee59c457")!
    fileprivate let metaIdentifier = "Oreum-Meta"
    fileprivate var notificationTimer: Timer? = nil
    fileprivate let notificationInterval: Double = 1.0
    fileprivate var rangedBeaconStabilizer: TTLAvgRangedBeaconStabilizer!

    fileprivate var _delegate: OreumBeaconManagerDelegate?
    var delegate: OreumBeaconManagerDelegate? {
        get {
            return _delegate
        }

        set {
            _delegate = newValue
        }
    }

    fileprivate var _automaticMonitoring: Bool = false
    var automaticMonitoring: Bool {
        get {
            return _automaticMonitoring
        }

        set {
            _automaticMonitoring = newValue
        }
    }

    fileprivate var _usingSimulation: Bool = false
    var usingSimulation: Bool {
        get {
            return _usingSimulation
        }

        set {
            _usingSimulation = newValue
        }
    }

    fileprivate var _deviceName: String?
    var deviceName: String? {
        get {
            return _deviceName
        }

        set {
            _deviceName = newValue
        }
    }

    fileprivate var _sensitivity: Sensitivity = .balanced
    var sensitivity: Sensitivity {
        get {
            return _sensitivity
        }

        set {
            _sensitivity = newValue
        }
    }

    override init() {
        super.init()
        beaconManager.delegate = self
        notificationTimer = Timer.scheduledTimer(timeInterval: notificationInterval,
                                                                   target: self,
                                                                   selector: #selector(notifyRangeBeacons(_:)),
                                                                   userInfo: nil,
                                                                   repeats: true)
        logger.debug("OreumBeaconService initilaize.")
    }

    @objc func notifyRangeBeacons(_ timer: Timer) {
        let stabledBeacons = rangedBeaconStabilizer.stabledBeacons()
        if stabledBeacons.count > 0 {
            _delegate?.oreumBeaconManager?(didRangeBeacons: stabledBeacons)
        }
    }

    func start() {
        switch _sensitivity {
        case .responsive:
            rangedBeaconStabilizer = TTLAvgRangedBeaconStabilizer(ttlSecond: 2.0, minRangeCount: 1)
        case .accurate:
            rangedBeaconStabilizer = TTLAvgRangedBeaconStabilizer(ttlSecond: 7.0, minRangeCount: 4)
        case .balanced:
            rangedBeaconStabilizer = TTLAvgRangedBeaconStabilizer(ttlSecond: 5.0, minRangeCount: 3)
        }

        if _automaticMonitoring {
            beaconManager.startMonitoring(metaUUID, identifier: metaIdentifier)
            logger.info("Meta Region Monitoring is started to Automatic Monitoring.")
        }

        if _usingSimulation {
            LocationSimulator(beaconManager: beaconManager).start(_deviceName!)
        } else {
            beaconManager.startUpdatingLocation()
        }
    }

    func startMonitoring() {
        repository.regionDatas {
            regionDatas in
            if let regionDatas = regionDatas {
                regionDatas.forEach {
                    self.startMonitoring($0)
                }
            }
        }
    }

    func startMonitoring(forId regionId: Int) {
        repository.regionData(of: regionId) {
            self.startMonitoring($0)
        }
    }

    func stopMonitoring() {
        repository.regionDatas {
            regionDatas in
            if let regionDatas = regionDatas {
                regionDatas.forEach {
                    self.stopMonitoring($0)
                }
            }
        }
    }

    func stopMonitoring(forId regionId: Int) {
        repository.regionData(of: regionId) {
            self.stopMonitoring($0)
        }
    }

    fileprivate func startMonitoring(_ regionData: RegionData?) {
        let region = TamraRegion(regionData: regionData)

        guard let uuid = region.uuid else {
            logger.error("Invalid Region's Id : \(region.id)")
            return
        }

        guard let name = region.name else {
            logger.error("Invalid Region's Id : \(region.id)")
            return
        }

        guard uuid as UUID != metaUUID else {
            return
        }

        guard !beaconManager.isOnMonitor(uuid) else {
            return
        }

        beaconManager.startMonitoring(uuid, identifier: name)
        sdkLogger.log("startMonitoring", param: String(region.id!))
        logger.info("Request to Start Region Monitoring. -> \(name)")
    }

    fileprivate func stopMonitoring(_ regionData: RegionData?) {
        guard let regionData = regionData else {
            return
        }

        guard regionData.uuid as UUID != metaUUID else {
            return
        }

        guard beaconManager.isOnMonitor(regionData.uuid) else {
            return
        }

        beaconManager.stopMonitoring(regionData.uuid, identifier: regionData.name)
        sdkLogger.log("stopMonitoring", param: String(regionData.id))
        logger.info("Request to Stop Region Monitoring. -> \(regionData.name)")
    }

}

extension OreumBeaconManager: BeaconManagerDelegate {

    func beaconManager(didEnterRegion region: Region) {
        guard region.uuid as UUID != metaUUID else {
            return
        }

        repository.regionData(of: region.uuid) {
            regionData in
            if let regionData = regionData {
                self._delegate?.oreumBeaconManager?(didEnterRegion: regionData)
                self.sdkLogger.log("didEnterRegion", param: String(regionData.id))
            }
        }

    }

    func beaconManager(didExitRegion region: Region) {
        guard region.uuid as UUID != metaUUID else {
            return
        }

        repository.regionData(of: region.uuid) {
            regionData in
            if let regionData = regionData {
                self._delegate?.oreumBeaconManager?(didExitRegion: regionData)
                self.sdkLogger.log("didExitRegion", param: String(regionData.id))
            }
        }
    }

    func beaconManager(didRangeBeacons beacons: [Beacon], inRegion region: Region) {
        if _usingSimulation && !region.simulated {
            return
        }

        if region.uuid as UUID == metaUUID {
            for beacon in beacons {
                repository.regionData(forMetaBeaconKey: BeaconKey(beacon: beacon)) {
                    regionData in
                    self.startMonitoring(regionData)
                }
            }

            return
        }

        let now = Date()
        let rangedBeacons = beacons.map {
            beacon in
            return RangedBeacon(region: region, beacon: beacon, rangedAt: now)
        }

        rangedBeaconStabilizer.ranged(rangedBeacons)
    }

    func beaconManager(didUpdateLocation location: Location) {
        if !_automaticMonitoring {
            return
        }

        repository.regionDatas {
            regionDatas in
            if let regionDatas = regionDatas {
                for regionData in regionDatas {
                    let regionLocation = Location(latitude: regionData.latitude, longitude: regionData.longitude)
                    if location.distanceFromLocation(regionLocation) < Double(regionData.radius) {
                        self.startMonitoring(regionData)
                    } else {
                        self.stopMonitoring(regionData)
                    }
                }
            }
        }
    }

}

@objc
protocol OreumBeaconManagerDelegate: NSObjectProtocol {

    @objc optional func oreumBeaconManager(didEnterRegion regionData: RegionData)

    @objc optional func oreumBeaconManager(didExitRegion regionData: RegionData)

    @objc optional func oreumBeaconManager(didRangeBeacons beacons: [RangedBeacon])

}

enum Sensitivity {
    case balanced
    case responsive
    case accurate
}
