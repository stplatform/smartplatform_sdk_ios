import Foundation
import RealmSwift
import Alamofire

protocol SDKLogger {

    func log(_ action: String, param: String)

    func sync()

}

class SDKLoggers {

    static var defaultLogger: SDKLogger {
        return persistentLogger
    }

    static var dummyLogger: SDKLogger {
        return DummySDKLogger.sharedInstance
    }

    static var persistentLogger: SDKLogger {
        return PersistentSDKLogger.sharedInstance
    }

    fileprivate class DummySDKLogger: SDKLogger {

        static let sharedInstance = DummySDKLogger()

        fileprivate init() {

        }

        func log(_ action: String, param: String) {
            // do nothing
        }

        func sync() {
            // do nothing
        }

    }

    fileprivate class PersistentSDKLogger: NSObject, SDKLogger, APISupport {

        static let sharedInstance = PersistentSDKLogger()
        fileprivate var realm: Realm = RealmAgent.sharedInstance.defaultRealm
        fileprivate let consoleLogger: Logger = Logger(name: String(describing: PersistentSDKLogger.self))
        var storedToken: Token? = nil

        fileprivate override init() {
            super.init()
            Timer.scheduledTimer(timeInterval: 300.0,
                                                   target: self,
                                                   selector: #selector(sync),
                                                   userInfo: nil,
                                                   repeats: true)
        }

        func storeToken(_ token: Token?) {
            storedToken = token
        }

        func log(_ action: String, param: String) {
            let sdkLog = SDKLog.create(action, param: param)
            try! realm.write {
                realm.add(sdkLog)
            }
        }

        @objc
        func sync() {
            consoleLogger.info("start sync.")
            let results = realm.objects(SDKLog.self)

            if results.isEmpty {
                return
            }

            withAuthentication {
                authentication in
                let request = NSMutableURLRequest(url: URL(string: self.apiURL("/sdklog") )!)
                for (key, value) in authentication.headers {
                    request.setValue(value, forHTTPHeaderField: key)
                }
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.httpMethod = "POST"
                request.httpBody = try! JSONSerialization.data(withJSONObject: results.map { $0.dictionary }, options: [])

                Alamofire.request(request as! URLRequestConvertible)
                .validate(statusCode: 200 ..< 300)
                .responseData {
                    response in
                    switch response.result {
                    case .success:
                        self.consoleLogger.info("SDKLogs sync is succeed.")
                        try! self.realm.write {
                            self.realm.delete(results)
                        }
                    case .failure(let error):
                        self.consoleLogger.info("SDKLogs sync is failed. --> \(error)")
                    }
                }
            }
        }

    }

}

class SDKLog: Object {

    @objc dynamic var action: String = "none"
    @objc dynamic var param: String = "none"
    @objc dynamic var issuedAt: Date = Date()

    static func create(_ action: String, param: String) -> SDKLog {
        let log = SDKLog()
        log.action = action
        log.param = param
        return log
    }

    var dictionary: [String:AnyObject] {
        var dict: [String:AnyObject] = [:]
        dict["action"] = action as AnyObject
        dict["param"] = param as AnyObject
        dict["issuedAt"] = issuedAt.utcString() as AnyObject
        return dict
    }

}
