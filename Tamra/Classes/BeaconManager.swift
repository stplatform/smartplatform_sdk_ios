import Foundation
import CoreLocation

class BeaconManager: NSObject {

    fileprivate let logger: Logger = Logger(name: String(describing: BeaconManager.self))
    fileprivate let locationManager = CLLocationManager()

    fileprivate var _monitoringRegions: [UUID:CLBeaconRegion] = [:]
    var monitoringRegions: [CLBeaconRegion] {
        return Array(_monitoringRegions.values);
    }

    fileprivate var _delegate: BeaconManagerDelegate?
    var delegate: BeaconManagerDelegate? {
        get {
            return _delegate
        }

        set {
            _delegate = newValue
        }
    }

    override init() {
        super.init()
        locationManager.delegate = self
    }

    func startMonitoring(_ uuid: UUID, identifier: String) {
        let region = CLBeaconRegion(proximityUUID: uuid, identifier: identifier)
        locationManager.startMonitoring(for: region)
        locationManager.requestState(for: region)
        _monitoringRegions[uuid] = region
    }

    func stopMonitoring(_ uuid: UUID, identifier: String) {
        let region = CLBeaconRegion(proximityUUID: uuid, identifier: identifier)
        locationManager.stopMonitoring(for: region)
        locationManager.stopRangingBeacons(in: region)
        _monitoringRegions.removeValue(forKey: uuid)
    }

    func startUpdatingLocation() {
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.distanceFilter = 500.0
        locationManager.startUpdatingLocation()
    }

    func isOnMonitor(_ uuid: UUID) -> Bool {
        return _monitoringRegions[uuid] != nil
    }

    func notifyRangedBeacons(didRangeBeacons beacons: [Beacon], inRegion region: Region) {
        if isOnMonitor(region.uuid) {
            _delegate?.beaconManager?(didRangeBeacons: beacons, inRegion: region)
        }
    }

    func notifyEnterRegion(_ region: Region) {
        if isOnMonitor(region.uuid) {
            _delegate?.beaconManager?(didEnterRegion: region)
        }
    }

    func notifyExitRegion(_ region: Region) {
        if isOnMonitor(region.uuid) {
            _delegate?.beaconManager?(didExitRegion: region)
        }
    }

}

extension BeaconManager: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager,
                         didStartMonitoringFor region: CLRegion) {
        logger.debug("Region monitoring is started. (region: %@)", region.description)
    }

    func locationManager(_ manager: CLLocationManager,
                         didDetermineState state: CLRegionState,
                         for region: CLRegion) {
        if state == .inside {
            locationManager(manager, didEnterRegion: region)
        }
    }

    func locationManager(_ manager: CLLocationManager,
                         didEnterRegion region: CLRegion) {
        logger.debug("Entered region. (region: %@)", region.description)
        let beaconRegion = region as! CLBeaconRegion
        manager.startRangingBeacons(in: beaconRegion)

        _delegate?.beaconManager?(didEnterRegion: Region(region: region))
    }

    func locationManager(_ manager: CLLocationManager,
                         didExitRegion region: CLRegion) {
        logger.debug("Exited region. (region: %@)", region.description)
        let beaconRegion = region as! CLBeaconRegion
        manager.stopRangingBeacons(in: beaconRegion)

        _delegate?.beaconManager?(didExitRegion: Region(region: region))
    }

    func locationManager(_ manager: CLLocationManager,
                         didRangeBeacons beacons: [CLBeacon],
                         in beaconRegion: CLBeaconRegion) {
        logger.debug("Ranged Beacons. (beacons count: %d, region: %@)", beacons.count, beaconRegion.description)

        let converted = beacons.map {
            beacon in
            return Beacon(beacon: beacon)
        }

        let region = Region(beaconRegion: beaconRegion)

        notifyRangedBeacons(didRangeBeacons: converted, inRegion: region)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = manager.location {
            logger.debug("update location. (latitude: \(location.coordinate.latitude), longitude: \(location.coordinate.longitude)")
            _delegate?.beaconManager?(didUpdateLocation: Location(location: location))
        }
    }

}

@objc
protocol BeaconManagerDelegate: NSObjectProtocol {

    @objc optional func beaconManager(didEnterRegion region: Region)

    @objc optional func beaconManager(didExitRegion region: Region)

    @objc optional func beaconManager(didRangeBeacons beacons: [Beacon], inRegion region: Region)

    @objc optional func beaconManager(didUpdateLocation location: Location)

}

class Beacon: NSObject {

    let _uuid: UUID
    var uuid: UUID {
        return _uuid
    }

    let _major: Int
    var major: Int {
        return _major
    }

    let _minor: Int
    var minor: Int {
        return _minor
    }

    let _accuracy: Double
    var accuracy: Double {
        return _accuracy
    }

    let _proximity: Proximity
    var proximity: Proximity {
        return _proximity
    }

    init(beacon: CLBeacon) {
        _uuid = beacon.proximityUUID
        _major = beacon.major.intValue
        _minor = beacon.minor.intValue
        _accuracy = beacon.accuracy
        _proximity = Proximity.from(beacon.proximity.stringValue)!
        super.init()
    }

    init(uuid: UUID,
         major: Int,
         minor: Int,
         accuracy: Double) {
        _uuid = uuid
        _major = major
        _minor = minor
        _accuracy = accuracy

        switch accuracy {
        case 0 ... 1:
            _proximity = .immediate
        case 1 ... 3:
            _proximity = .near
        case 3 ... 10:
            _proximity = .far
        default:
            _proximity = .unknown
        }

        super.init()
    }

}

class Region: NSObject {

    let _uuid: UUID
    var uuid: UUID {
        return _uuid
    }

    let _identifier: String
    var identifier: String {
        return _identifier
    }

    var simulated: Bool {
        return _identifier.hasPrefix(LocationSimulator.identifierPrefix)
    }

    init(region: CLRegion) {
        _uuid = (region as! CLBeaconRegion).proximityUUID
        _identifier = region.identifier
        super.init()
    }

    init(beaconRegion: CLBeaconRegion) {
        _uuid = beaconRegion.proximityUUID
        _identifier = beaconRegion.identifier
        super.init()
    }

    init(uuid: UUID,
         identifier: String) {
        _uuid = uuid
        _identifier = identifier
        super.init()
    }

}

class Location: NSObject {

    let latitude: Double
    let longitude: Double

    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }

    init(location: CLLocation) {
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
    }

    func distanceFromLocation(_ location: Location) -> Double {
        let me = CLLocation(latitude: latitude, longitude: longitude)
        let another = CLLocation(latitude: location.latitude, longitude: location.longitude)
        return me.distance(from: another)
    }

}

enum Proximity {
    case immediate
    case near
    case far
    case unknown

    static func from(_ raw: String) -> Proximity? {
        switch raw {
        case "Unknown":
            return .unknown
        case "Immediate":
            return .immediate
        case "Near":
            return .near
        case "Far":
            return .far
        default:
            return nil
        }
    }

    var stringValue: String {
        switch self {
        case .unknown:
            return "Unknown"
        case .immediate:
            return "Immediate"
        case .near:
            return "Near"
        case .far:
            return "Far"
        }
    }

}
