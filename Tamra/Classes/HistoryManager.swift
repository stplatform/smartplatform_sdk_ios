import Foundation
import RealmSwift

// TODO Interface 레이어로 옮겨가야되지 않을까?
protocol HistoryManager {

    func fetch(_ period: TamraHistoryPeriod) -> TamraHistories

    func last(_ n: Int) -> TamraHistories

    func hasVisited(_ spot: TamraSpot) -> TamraHistories

    func log(_ spot: TamraSpot)

    func clear()

}

class BasicHistoryManager: HistoryManager {

    fileprivate let realm: Realm = RealmAgent.sharedInstance.defaultRealm

    func fetch(_ period: TamraHistoryPeriod) -> TamraHistories {
        let results = Array(realm.objects(HistoryForRealm.self).filter {
            history in
            return history.issuedAt.compare(period.from) == .orderedDescending &&
                    history.issuedAt.compare(period.to) == .orderedAscending
        }.map { $0.tamraHistory })
        
        return TamraHistories(dataArray: results)
    }

    func last(_ n: Int) -> TamraHistories {
        let results = realm.objects(HistoryForRealm.self).sorted(byKeyPath: "issuedAt", ascending: false)

        if results.count < n {
            return TamraHistories(dataArray: results.map { $0.tamraHistory })
        }

        return TamraHistories(dataArray: results[0 ..< n].map { $0.tamraHistory })
    }

    func hasVisited(_ spot: TamraSpot) -> TamraHistories {
        let results = Array(realm.objects(HistoryForRealm.self).filter {
            history in
            return history.spotId == spot.id
        }.map { $0.tamraHistory })

        return TamraHistories(dataArray: results)
    }

    func log(_ spot: TamraSpot) {
        let history = HistoryForRealm.create(for: spot)
        try! realm.write {
            realm.add(history)
        }
    }

    func clear() {
        try! realm.write {
            realm.delete(realm.objects(HistoryForRealm.self))
        }
    }

}

class HistoryForRealm: Object {

    @objc dynamic var spotId: Int = RealmDefaultValue.int
    @objc dynamic var desc: String = RealmDefaultValue.string
    @objc dynamic var proximity: String = RealmDefaultValue.string
    @objc dynamic var accuracy: Double = RealmDefaultValue.double
    @objc dynamic var latitude: Double = RealmDefaultValue.double
    @objc dynamic var longitude: Double = RealmDefaultValue.double
    @objc dynamic var issuedAt: Date = Date()

    static func create(for spot: TamraSpot) -> HistoryForRealm {
        let history = HistoryForRealm()
        history.spotId = spot.id
        history.desc = spot.desc
        history.proximity = spot.proximity.stringValue
        history.accuracy = spot.accuracy
        history.latitude = spot.latitude
        history.longitude = spot.longitude
        return history
    }

    var tamraHistory: TamraHistory {
        return TamraHistory(spotId: spotId,
                            description: desc,
                            issuedAt: issuedAt)
    }

}
