import Foundation

open class TamraNearbySpots: NSObject, Sequence {

    fileprivate static let logger = Logger(name: String(describing: TamraNearbySpots.self))
    fileprivate static let repository = DataRepository.sharedInstance

    fileprivate let spots: [TamraSpot]

    open var size: Int {
        return spots.count
    }

    open var first: TamraSpot? {
        return spots.first
    }

    open var last: TamraSpot? {
        return spots.last
    }

    override init() {
        self.spots = []
    }

    init(spots: [TamraSpot]) {
        self.spots = spots
    }

    open func makeIterator() -> IndexingIterator<Array<TamraSpot>> {
        return spots.makeIterator()
    }

    open func filter(_ predicate: (TamraSpot) -> Bool) -> TamraNearbySpots {
        return TamraNearbySpots(spots: spots.filter(predicate))
    }

    open func orderBy(_ comparator: (TamraSpot, TamraSpot) -> Bool) -> TamraNearbySpots {
        return TamraNearbySpots(spots: spots.sorted(by: comparator))
    }

    open func all(_ predicate: (TamraSpot) -> Bool) -> Bool {
        for spot in spots {
            if !predicate(spot) {
                return false
            }
        }

        return true
    }

    open func any(_ predicate: (TamraSpot) -> Bool) -> Bool {
        for spot in spots {
            if predicate(spot) {
                return true
            }
        }

        return false
    }

    open func transform<T>(_ converter: (TamraSpot) -> T) -> [T] {
        return spots.map {
            spot in
            return converter(spot)
        }
    }

    open func index<T>(_ indexer: (TamraSpot) -> T) -> [T:[TamraSpot]] {
        var dict: [T:[TamraSpot]] = [:]

        for spot in spots {
            let key = indexer(spot)
            if dict[key] == nil {
                dict[key] = []
            }

            dict[key]! += [spot]
        }

        return dict
    }

    open func toMap<T>(_ mapper: (TamraSpot) -> T) -> [TamraSpot:T] {
        var dict: [TamraSpot:T] = [:]

        for spot in spots {
            dict[spot] = mapper(spot)
        }

        return dict
    }

    static func fetch(_ rangedBeacons: [RangedBeacon], done: @escaping (TamraNearbySpots) -> Void) {
        let dictByBeaconKey: [BeaconKey:RangedBeacon] = rangedBeacons.reduce([BeaconKey: RangedBeacon]()) {
            (dict, rangedBeacon) in
            var dict = dict
            let beaconKey = rangedBeacon.beaconKey
            dict[beaconKey] = rangedBeacon
            return dict
        }

        TamraNearbySpots.repository.beaconDatas(of: Array(dictByBeaconKey.keys)) {
            beaconDatas in
            if let beaconDatas = beaconDatas {
                let tamraSpots: [TamraSpot] = beaconDatas.map {
                    beaconData in
                    let rangedBeacon = dictByBeaconKey[beaconData.beaconKey]!
                    return TamraSpot(
                            beaconData: beaconData,
                            region: rangedBeacon.region,
                            proximity: TamraProximity.from(rangedBeacon.proximity)!,
                            accuracy: rangedBeacon.accuracy)
                }

                done(TamraNearbySpots(spots: tamraSpots))
            } else {
                done(TamraNearbySpots())
            }
        }
    }

}

open class TamraSpot {

    fileprivate static let dataRepository = DataRepository.sharedInstance

    fileprivate let beaconData: BeaconData
    fileprivate let region: Region
    open let proximity: TamraProximity
    open let accuracy: Double

    open var id: Int {
        return beaconData.id
    }

    open var desc: String {
        return beaconData.desc
    }

    open var indoor: Bool {
        return beaconData.indoor
    }

    open var movable: Bool {
        return beaconData.movable
    }

    open var latitude: Double {
        return beaconData.latitude
    }

    open var longitude: Double {
        return beaconData.longitude
    }

    open var geohash: String {
        return beaconData.geohash
    }

    open var regionName: String {
        return region.identifier
    }

    open func inRegion(_ tamraRegion: TamraRegion) -> Bool {
        return region.uuid == tamraRegion.uuid
    }

    init(beaconData: BeaconData, region: Region, proximity: TamraProximity = .unknown, accuracy: Double = 0.0) {
        self.beaconData = beaconData
        self.region = region
        self.proximity = proximity
        self.accuracy = accuracy
    }

    func from(rangedBeacon: RangedBeacon, done: @escaping (TamraSpot?) -> Void) {
        let beaconKey = rangedBeacon.beaconKey
        TamraSpot.dataRepository.beaconData(of: beaconKey) {
            beaconData in
            if let beaconData = beaconData {
                let spot = TamraSpot(
                        beaconData: beaconData,
                        region: rangedBeacon.region,
                        proximity: TamraProximity.from(rangedBeacon.proximity)!,
                        accuracy: rangedBeacon.accuracy)
                done(spot)
            } else {
                done(nil)
            }
        }
    }

    open func data(_ done: @escaping
        (String?) -> Void) {
        TamraSpot.dataRepository.spots(of: id, done: done)
    }

    // 모두 오름차순
    open static let ID_ORDER: (TamraSpot, TamraSpot) -> Bool = { $0.id < $1.id }
    open static let ACCURACY_ORDER: (TamraSpot, TamraSpot) -> Bool = { $0.accuracy < $1.accuracy }
    open static let PROXIMITY_ORDER: (TamraSpot, TamraSpot) -> Bool = { $0.proximity.rawValue < $1.proximity.rawValue }

}

extension TamraSpot: Hashable {

    // TODO 이렇게만 해도 될까?
    public var hashValue: Int {
        return id
    }

}

extension TamraSpot: Equatable {

}

public func ==(lhs: TamraSpot, rhs: TamraSpot) -> Bool {
    return lhs.id == rhs.id
}

public enum TamraProximity: Int {
    case immediate = 1
    case near = 3
    case far = 10
    case unknown = 9999

    static func from(_ raw: String) -> TamraProximity? {
        switch raw {
        case "Unknown":
            return .unknown
        case "Immediate":
            return .immediate
        case "Near":
            return .near
        case "Far":
            return .far
        default:
            return nil
        }
    }

    var stringValue: String {
        switch self {
        case .unknown:
            return "Unknown"
        case .immediate:
            return "Immediate"
        case .near:
            return "Near"
        case .far:
            return "Far"
        }
    }

}

open class TamraSpotFilters {

    open static func belongsTo(_ region: TamraRegion) -> (TamraSpot) -> Bool {
        return {
            spot in
            return spot.inRegion(region)
        }
    }

    open static func maxProximity(_ proximity: TamraProximity) -> (TamraSpot) -> Bool {
        return {
            spot in
            // TODO 이게 아닌거 같은데..
            return spot.proximity == proximity
        }
    }

    open static func oneOf(_ spots: TamraNearbySpots) -> (TamraSpot) -> Bool {
        return {
            spot in
            return spots.spots.contains(spot)
        }
    }

}
