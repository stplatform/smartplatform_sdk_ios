import Foundation
import CoreLocation
import CoreBluetooth
import ReachabilitySwift

class DeviceChecker: NSObject, CLLocationManagerDelegate, CBPeripheralManagerDelegate {

    static let sharedInstance = DeviceChecker()
    let logger = Logger(name: String(describing: DeviceChecker.self))

    fileprivate var _locationStatus: TamraLocationStatus = .unknown
    var location: TamraLocationStatus {
        get {
            return _locationStatus
        }
    }

    fileprivate var _bluetoothStatus: TamraBluetoothStatus = .unknown
    var bluetooth: TamraBluetoothStatus {
        get {
            return _bluetoothStatus
        }
    }

    fileprivate var _internetStatus: TamraInternetStatus = .unknown
    var internet: TamraInternetStatus {
        get {
            return _internetStatus
        }
    }

    var internetConnectable: Bool {
        return ([.wiFi, .cellular]).contains(internet)
    }

    fileprivate var locationManager: CLLocationManager?
    fileprivate var bluetoothManager: CBPeripheralManager?
    fileprivate var reachability: Reachability?

    fileprivate override init() {
        super.init()
        initLocationChecking()
        initBluetoothChecking()
        initInternetChecking()
    }

    deinit {
        if let reachability = reachability {
            reachability.stopNotifier()
        }
    }

    fileprivate func initLocationChecking() {
        locationManager = CLLocationManager()
        if let manager = locationManager {
            manager.delegate = self
        }
    }

    func locationManager(_ manager: CLLocationManager,
                         didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            logger.info("Location is Available")
            _locationStatus = .available
        case .notDetermined, .denied, .restricted:
            logger.info("Location is Unavailable")
            _locationStatus = .unavailable
        }
    }


    fileprivate func initBluetoothChecking() {
        bluetoothManager = CBPeripheralManager(delegate: self, queue: nil, options: nil)
    }

    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        switch peripheral.state {
        case .poweredOn:
            logger.info("Bluetooth is On")
            _bluetoothStatus = .on
        case .poweredOff:
            logger.info("Bluetooth is Off")
            _bluetoothStatus = .off
        case .unsupported, .unauthorized:
            logger.info("Bluetooth is Unavailable")
            _bluetoothStatus = .unavailable
        default:
            logger.info("Bluetooth Status is Unknown")
            _bluetoothStatus = .unknown
        }
    }

    fileprivate func initInternetChecking() {
        /*let reachability: Reachability
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            logger.debug("Unable to create Reachability for internet connection")
            return
        }*/
        let reachability = Reachability()

        reachability?.whenReachable = {
            reachability in
            if reachability.isReachableViaWiFi {
                self._internetStatus = .wiFi
                self.logger.debug("Internet is reachable with WiFi")
            } else {
                self._internetStatus = .cellular
                self.logger.debug("Internet is reachable with Cellular")
            }
        }

        reachability?.whenUnreachable = {
            reachability in
            self._internetStatus = .notReachable
            self.logger.debug("Internet is not reachable")
        }

        do {
            try reachability?.startNotifier()
        } catch {
            logger.debug("Unable to start notifier for internet connection")
        }

        self.reachability = reachability
    }

    func requestLocationAuthorization() {
        if _locationStatus == .available {
            return
        }

        if let manager = locationManager {
            manager.requestAlwaysAuthorization()
        }
    }

}
