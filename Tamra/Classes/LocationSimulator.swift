import Foundation
import CoreLocation

// TODO Data 레이어의 Object 들을 이용하고 있음

class LocationSimulator: NSObject {

    static let identifierPrefix = "__simulated-region__"
    fileprivate let logger = Logger(name: String(describing: LocationSimulator.self))
    fileprivate let dataSource: DataSource = NetworkDataSource.sharedInstance
    fileprivate let beaconManager: BeaconManager
    fileprivate let interval: Double = 0.5
    fileprivate var timerForGetSignal: Timer!
    fileprivate var timerForCheckExitRegion: Timer!
    fileprivate var deviceName: String!
    fileprivate var enteredRegions: [UUID: Date] = [:]

    init(beaconManager: BeaconManager) {
        self.beaconManager = beaconManager
    }

    func start(_ deviceName: String) {
        self.deviceName = deviceName
        timerForGetSignal = Timer.scheduledTimer(timeInterval: interval,
                                                                   target: self,
                                                                   selector: #selector(getSignal),
                                                                   userInfo: nil,
                                                                   repeats: true)
        timerForCheckExitRegion = Timer.scheduledTimer(timeInterval: interval,
                                                                         target: self,
                                                                         selector: #selector(checkExitRegion),
                                                                         userInfo: nil,
                                                                         repeats: true)
        logger.info("LocationSimulator is started.")
    }

    func stop() {
        logger.info("LocationSimulator is stopped.")
        timerForGetSignal.invalidate()
        timerForGetSignal = nil
        timerForCheckExitRegion.invalidate()
        timerForCheckExitRegion = nil
    }

    @objc func checkExitRegion() {
        let now = Date()
        for (uuid, date) in enteredRegions {
            if now.timeIntervalSince(date) > 60 {
                enteredRegions[uuid] = nil
                let region = Region(uuid: uuid,
                                    identifier: "\(LocationSimulator.identifierPrefix)\(uuid.uuidString)")
                beaconManager.notifyExitRegion(region)
            }
        }
    }

    @objc func getSignal() {
        dataSource.simulatedSignals(deviceName) {
            signals in
            if let _signals = signals {
                let grouped: [UUID: [SimulatedSignal]] = _signals.reduce([UUID: [SimulatedSignal]]()) {
                    (dict, signal) in
                    var dict = dict
                    let uuid: UUID = signal.uuid

                    if dict[uuid] != nil {
                        dict[uuid]! += [signal]
                    } else {
                        dict[uuid] = [signal]
                    }

                    return dict
                }

                for (uuid, __signals) in grouped {
                    let beacons: [Beacon] = __signals.map {
                        signal in
                        self.logger.debug("signal: \(signal.id) -> \(signal.accuracy)")
                        return Beacon(uuid: signal.uuid,
                                      major: signal.major,
                                      minor: signal.minor,
                                      accuracy: signal.accuracy)
                    }

                    let region = Region(uuid: uuid,
                                        identifier: "\(LocationSimulator.identifierPrefix)\(uuid.uuidString)")

                    if self.enteredRegions[uuid] == nil {
                        self.beaconManager.notifyEnterRegion(region)
                    }

                    self.enteredRegions[uuid] = Date()
                    self.beaconManager.notifyRangedBeacons(didRangeBeacons: beacons, inRegion: region)
                }
            }
        }
    }

}
