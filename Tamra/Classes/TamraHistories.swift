import Foundation

open class TamraHistories: Sequence {

    let dataArray: [TamraHistory]

    init() {
        dataArray = []
    }

    init(dataArray: [TamraHistory]) {
        self.dataArray = dataArray
    }

    open func makeIterator() -> IndexingIterator<Array<TamraHistory>> {
        return dataArray.makeIterator()
    }

    open var first: TamraHistory? {
        return dataArray.first
    }

    open var last: TamraHistory? {
        return dataArray.last
    }

    open var count: Int {
        return dataArray.count
    }

}

open class TamraHistory {

    open let spotId: Int
    open let description: String
    open let issuedAt: Date

    init(spotId: Int,
         description: String,
         issuedAt: Date = Date()) {
        self.spotId = spotId
        self.description = description
        self.issuedAt = issuedAt
    }

}

open class TamraHistoryPeriod {

    let from: Date
    let to: Date

    init(from: Date, to: Date) {
        self.from = from
        self.to = to
    }

    public convenience init(lastSeconds amount: Int) {
        self.init(from: Date().addingTimeInterval(-Double(amount)), to: Date())
    }

    public convenience init(lastMinutes amount: Int) {
        self.init(lastSeconds: amount * 60)
    }

    public convenience init(lastHours amount: Int) {
        self.init(lastMinutes: amount * 60)
    }

}


