import Foundation

class RegionData: NSObject {

    fileprivate let logger = Logger(name: String(describing: RegionData.self))

    let id: Int
    let name: String
    let regionType: String
    let uuid: UUID
    let latitude: Double
    let longitude: Double
    let geohash: String
    let radius: Int
    let desc: String

    init(id: Int,
         name: String,
         regionType: String,
         uuid: UUID,
         latitude: Double,
         longitude: Double,
         geohash: String,
         radius: Int,
         desc: String) {
        self.id = id
        self.name = name
        self.regionType = regionType
        self.uuid = uuid
        self.latitude = latitude
        self.longitude = longitude
        self.geohash = geohash
        self.radius = radius
        self.desc = desc
        super.init()
    }

    init(dictionary: Dictionary<String, AnyObject>) {
        self.id = dictionary["id"] as! Int
        self.name = dictionary["name"] as! String
        self.regionType = dictionary["regionType"] as! String
        self.uuid = UUID(uuidString: dictionary["uuid"] as! String)!
        self.latitude = dictionary["latitude"] as! Double
        self.longitude = dictionary["longitude"] as! Double
        self.geohash = dictionary["geohash"] as! String
        self.radius = dictionary["radius"] as! Int
        self.desc = dictionary["description"] as! String
        super.init()
    }

}
