import Foundation

class DataRepository {

    fileprivate let logger = Logger(name: String(describing: DataRepository.self))

    static let sharedInstance = DataRepository()

    fileprivate let deviceChecker = DeviceChecker.sharedInstance
    fileprivate let dataSource: DataSource = NetworkDataSource.sharedInstance
    fileprivate var dataCache: DataCache = DataCaches.createDefault()

    fileprivate init() {

    }

    var acceptable: Bool {
        return dataCache.acceptable
    }

    func reload(_ done: @escaping (Bool) -> Void) {
        logger.info("Data Reloading is started.")
        updateRegionDatas(onlyWifi: false) {
            regionUpdated in
            self.logger.info("Region Data Updating Result : \(regionUpdated)")
            self.updateBeaconDatas(onlyWifi: false) {
                beaconUpdated in
                self.logger.info("Beacon Data Updating Result : \(beaconUpdated)")
                done(regionUpdated && beaconUpdated)
            }
        }
    }

    func regionDatas(_ done: @escaping ([RegionData]?) -> Void) {
        if let regionDatas = dataCache.regionDatas {
            done(regionDatas)
        } else {
            updateRegionDatas {
                _ in
                done(self.dataCache.regionDatas)
            }
        }
    }

    func regionData(of id: Int, done: @escaping (RegionData?) -> Void) {
        if let regionData = dataCache.regionData(byId: id) {
            done(regionData)
        } else {
            updateRegionDatas {
                _ in
                done(self.dataCache.regionData(byId: id))
            }
        }
    }

    func regionData(of uuid: UUID, done: @escaping (RegionData?) -> Void) {
        if let regionData = dataCache.regionData(byUUID: uuid) {
            done(regionData)
        } else {
            updateRegionDatas {
                _ in
                done(self.dataCache.regionData(byUUID: uuid))
            }
        }
    }

    func regionData(forMetaBeaconKey beaconKey: BeaconKey, done: @escaping (RegionData?) -> Void) {
        beaconData(of: beaconKey) {
            beaconData in
            if let beaconData = beaconData {
                self.regionData(of: beaconData.regionId) {
                    regionData in
                    done(regionData)
                }
            } else {
                done(nil)
            }
        }
    }

    func beaconData(of id: Int, done: @escaping (BeaconData?) -> Void) {
        if let beaconData = dataCache.beaconData(byId: id) {
            done(beaconData)
        } else {
            updateBeaconDatas {
                _ in
                done(self.dataCache.beaconData(byId: id))
            }
        }
    }

    func beaconData(of key: BeaconKey, done: @escaping (BeaconData?) -> Void) {
        if let beaconData = dataCache.beaconData(byKey: key) {
            done(beaconData)
        } else {
            updateBeaconDatas {
                _ in
                done(self.dataCache.beaconData(byKey: key))
            }
        }
    }

    func beaconDatas(of ids: [Int], done: @escaping ([BeaconData]?) -> Void) {
        if let beaconDatas = dataCache.beaconDatas(byIds: ids) {
            done(beaconDatas)
        } else {
            updateBeaconDatas {
                _ in
                done(self.dataCache.beaconDatas(byIds: ids))
            }
        }
    }

    func beaconDatas(of keys: [BeaconKey], done: @escaping ([BeaconData]?) -> Void) {
        if let beaconDatas = dataCache.beaconDatas(byKeys: keys) {
            done(beaconDatas)
        } else {
            updateBeaconDatas {
                _ in
                done(self.dataCache.beaconDatas(byKeys: keys))
            }
        }
    }

    func spots(of beaconId: Int, done: @escaping (String?) -> Void) {
        dataSource.spot(byBeaconId: beaconId, done: done)
    }

    fileprivate func updateRegionDatas(_ done: ((Bool) -> Void)? = nil) {
        updateRegionDatas(onlyWifi: true, done: done)
    }

    fileprivate func updateRegionDatas(onlyWifi: Bool, done: ((Bool) -> Void)? = nil) {
        logger.info("RegionData is updated.")

        if onlyWifi && deviceChecker.internet != .wiFi {
            done?(false)
            return
        }

        dataSource.regionDatas {
            regionDatas in
            self.dataCache.regionDatas = regionDatas
            done?(regionDatas != nil)
        }
    }

    fileprivate func updateBeaconDatas(_ done: ((Bool) -> Void)? = nil) {
        updateBeaconDatas(onlyWifi: true, done: done)
    }

    fileprivate func updateBeaconDatas(onlyWifi: Bool, done: ((Bool) -> Void)? = nil) {
        logger.info("BeaconData is updated.")

        if onlyWifi && deviceChecker.internet != .wiFi {
            done?(false)
            return
        }

        dataSource.beaconDatas {
            beaconDatas in
            self.dataCache.beaconDatas = beaconDatas
            done?(beaconDatas != nil)
        }
    }

}
