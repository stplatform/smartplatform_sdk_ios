import Foundation
import RealmSwift

protocol DataCache {

    var regionDatas: [RegionData]? { get set }
    var beaconDatas: [BeaconData]? { get set }
    var acceptable: Bool { get }

    func regionData(byId id: Int) -> RegionData?

    func regionData(byUUID uuid: UUID) -> RegionData?

    func beaconData(byId id: Int) -> BeaconData?

    func beaconData(byKey key: BeaconKey) -> BeaconData?

    func beaconDatas(byIds ids: [Int]) -> [BeaconData]?

    func beaconDatas(byKeys keys: [BeaconKey]) -> [BeaconData]?

}

class DataCaches {

    static func createDefault() -> DataCache {
        return createUsingDisk()
    }

    static func createUsingMemory() -> DataCache {
        return MemoryDataCache()
    }

    static func createUsingDisk() -> DataCache {
        return RealmDataCache()
    }

}

class MemoryDataCache: DataCache {

    fileprivate let logger = Logger(name: String(describing: MemoryDataCache.self))

    var _regionDatas: [RegionData]? = nil
    var regionDatas: [RegionData]? {
        get {
            return _regionDatas
        }

        set {
            _regionDatas = newValue
        }
    }

    var _beaconDatas: [BeaconData]? = nil
    var beaconDatas: [BeaconData]? {
        get {
            return _beaconDatas
        }

        set {
            _beaconDatas = newValue
        }
    }

    var acceptable: Bool {
        return _regionDatas != nil
                && !_regionDatas!.isEmpty
                && _beaconDatas != nil
                && !_beaconDatas!.isEmpty
    }

    func regionData(byId id: Int) -> RegionData? {
        return _regionDatas?.filter {
            regionData in
            return regionData.id == id
        }.first
    }

    func regionData(byUUID uuid: UUID) -> RegionData? {
        return _regionDatas?.filter {
            regionData in
            return regionData.uuid as UUID == uuid
        }.first
    }

    func beaconData(byId id: Int) -> BeaconData? {
        return _beaconDatas?.filter {
            beaconData in
            return beaconData.id == id
        }.first
    }

    func beaconData(byKey key: BeaconKey) -> BeaconData? {
        return _beaconDatas?.filter {
            beaconData in
            return beaconData.beaconKey == key
        }.first
    }

    func beaconDatas(byIds ids: [Int]) -> [BeaconData]? {
        return _beaconDatas?.filter {
            beaconData in
            return ids.contains(beaconData.id)
        }
    }

    func beaconDatas(byKeys keys: [BeaconKey]) -> [BeaconData]? {
        return _beaconDatas?.filter {
            beaconData in

            return keys.contains(beaconData.beaconKey)
        }
    }

}

class RealmDataCache: DataCache {

    fileprivate let logger = Logger(name: String(describing: RealmDataCache.self))
    fileprivate let realm: Realm = RealmAgent.sharedInstance.defaultRealm

    var regionDatas: [RegionData]? {
        get {
            return realm.objects(RegionForRealm.self).map({$0.toData})
        }

        set {
            if let regionDatas = newValue {
                try! realm.write {
                    for regionData in regionDatas {
                        let region = RegionForRealm.fromData(regionData)
                        realm.add(region, update: true)

                    }
                }
                realm.refresh()
            }
        }
    }

    var beaconDatas: [BeaconData]? {
        get {
            return realm.objects(BeaconForRealm.self).map({$0.toData})
        }

        set {
            if let beaconDatas = newValue {
                try! realm.write {
                    for beaconData in beaconDatas {
                        let beacon = BeaconForRealm.fromData(beaconData)

                        realm.add(beacon, update: true)

                    }
                }
                realm.refresh()
            }
        }
    }

    var acceptable: Bool {
        return !realm.objects(RegionForRealm.self).isEmpty && !realm.objects(BeaconForRealm.self).isEmpty
    }

    func regionData(byId id: Int) -> RegionData? {
        return realm.object(ofType: RegionForRealm.self, forPrimaryKey: id)?.toData
    }

    func regionData(byUUID uuid: UUID) -> RegionData? {
        return realm.objects(RegionForRealm.self).filter {
            region in
            return region.uuid == uuid.uuidString
        }.first?.toData
    }

    func beaconData(byId id: Int) -> BeaconData? {
        //return realm.objectForPrimaryKey(BeaconForRealm.self, key: id)?.toData
        return realm.object(ofType: BeaconForRealm.self, forPrimaryKey: id)?.toData
    }

    func beaconData(byKey key: BeaconKey) -> BeaconData? {
        return realm.objects(BeaconForRealm.self).filter {
            beacon in
            return key == beacon.key
        }.first?.toData
    }

    func beaconDatas(byIds ids: [Int]) -> [BeaconData]? {
        if realm.objects(BeaconForRealm.self).isEmpty {
            return nil
        }

        return realm.objects(BeaconForRealm.self).filter {
            beacon in
            return ids.contains(beacon.id)
        }.map {
            beacon in
            return beacon.toData
        }
    }

    func beaconDatas(byKeys keys: [BeaconKey]) -> [BeaconData]? {
        if realm.objects(BeaconForRealm.self).isEmpty {
            return nil
        }

        return realm.objects(BeaconForRealm.self).filter {
            beacon in
            return keys.contains(beacon.key)
        }.map {
            beacon in
            return beacon.toData
        }

    }

}

class RegionForRealm: Object {

    @objc dynamic var id: Int = RealmDefaultValue.int
    @objc dynamic var name: String = RealmDefaultValue.string
    @objc dynamic var regionType: String = RealmDefaultValue.string
    @objc dynamic var uuid: String = RealmDefaultValue.string
    @objc dynamic var latitude: Double = RealmDefaultValue.double
    @objc dynamic var longitude: Double = RealmDefaultValue.double
    @objc dynamic var geohash: String = RealmDefaultValue.string
    @objc dynamic var radius: Int = RealmDefaultValue.int
    @objc dynamic var desc: String = RealmDefaultValue.string

    override static func primaryKey() -> String? {
        return "id"
    }

    static func fromData(_ data: RegionData) -> RegionForRealm {
        let region = RegionForRealm()
        region.id = data.id
        region.name = data.name
        region.regionType = data.regionType
        region.uuid = data.uuid.uuidString
        region.latitude = data.latitude
        region.longitude = data.longitude
        region.geohash = data.geohash
        region.radius = data.radius
        region.desc = data.desc

        return region
    }

    var toData: RegionData {
        return RegionData(
                id: id,
                name: name,
                regionType: regionType,
                uuid: UUID(uuidString: uuid)!,
                latitude: latitude,
                longitude: longitude,
                geohash: geohash,
                radius: radius,
                desc: desc)
    }

}

class BeaconForRealm: Object {

    @objc dynamic var id: Int = RealmDefaultValue.int
    @objc dynamic var regionId: Int = RealmDefaultValue.int
    @objc dynamic var beaconType: String = RealmDefaultValue.string
    @objc dynamic var uuid: String = RealmDefaultValue.string
    @objc dynamic var major: Int = RealmDefaultValue.int
    @objc dynamic var minor: Int = RealmDefaultValue.int
    @objc dynamic var indoor: Bool = RealmDefaultValue.bool
    @objc dynamic var movable: Bool = RealmDefaultValue.bool
    @objc dynamic var txPower: Int = RealmDefaultValue.int
    @objc dynamic var latitude: Double = RealmDefaultValue.double
    @objc dynamic var longitude: Double = RealmDefaultValue.double
    @objc dynamic var geohash: String = RealmDefaultValue.string
    @objc dynamic var desc: String = RealmDefaultValue.string

    override static func primaryKey() -> String? {
        return "id"
    }

    static func fromData(_ data: BeaconData) -> BeaconForRealm {
        let beacon = BeaconForRealm()
        beacon.id = data.id
        beacon.regionId = data.regionId
        beacon.beaconType = data.beaconType
        beacon.uuid = data.uuid.uuidString
        beacon.major = data.major
        beacon.minor = data.minor
        beacon.indoor = data.indoor
        beacon.movable = data.movable
        beacon.txPower = data.txPower
        beacon.latitude = data.latitude
        beacon.longitude = data.longitude
        beacon.geohash = data.geohash
        beacon.desc = data.desc

        return beacon
    }

    var toData: BeaconData {
        return BeaconData(
                id: id,
                regionId: regionId,
                beaconType: beaconType,
                uuid: UUID(uuidString: uuid)!,
                major: major,
                minor: minor,
                indoor: indoor,
                movable: movable,
                txPower: txPower,
                latitude: latitude,
                longitude: longitude,
                geohash: geohash,
                desc: desc)
    }

    var key: BeaconKey {
        return BeaconKey(
                uuid: UUID(uuidString: uuid)!,
                major: major,
                minor: minor)
    }

}
