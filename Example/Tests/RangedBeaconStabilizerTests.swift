import Quick
import Nimble
@testable import Tamra

class RangedBeaconStabilizerSpec: QuickSpec {
    override func spec() {
        describe("TTLAvgRangedBeaconStabilizer") {
            it("지정한 ttl에 따라 오래된 비콘이 제거되어야 한다.") {
                // given
                let stabilizer = TTLAvgRangedBeaconStabilizer(ttlSecond: 0.5, minRangeCount: 1)
                let rangedBeacon = RangedBeacon.testing(accuracy: 1.0)

                // when
                stabilizer.ranged([rangedBeacon])

                // then
                NSThread.sleepForTimeInterval(0.3)
                expect(stabilizer.stabledBeacons().count).to(equal(1))
                NSThread.sleepForTimeInterval(0.2)
                expect(stabilizer.stabledBeacons().count).to(equal(0))
            }

            it("지정한 minRangeCount에 따라 기준에 충족한 비콘 신호만 제공해야 한다.") {
                // given
                let stabilizer = TTLAvgRangedBeaconStabilizer(ttlSecond: 3, minRangeCount: 3)
                let rangedBeacon = RangedBeacon.testing(accuracy: 1.0)

                // when & then
                stabilizer.ranged([rangedBeacon, rangedBeacon])
                expect(stabilizer.stabledBeacons().count).to(equal(0))
                stabilizer.ranged([rangedBeacon])
                expect(stabilizer.stabledBeacons().count).to(equal(1))
            }
        }
    }
}