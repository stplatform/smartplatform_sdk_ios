import Quick
import Nimble
@testable import Tamra

class TamraNearbySpotsSpec: QuickSpec {
    var testSpots: TamraNearbySpots {
        let uuid = NSUUID()
        let region = Region(uuid: uuid, identifier: "test-region")
        let beaconData1 = BeaconData(id: 1,
                                    regionId: 1,
                                    beaconType: "Meta",
                                    uuid: uuid,
                                    major: 1,
                                    minor: 1,
                                    indoor: true,
                                    movable: false,
                                    txPower: -12,
                                    latitude: 127.1,
                                    longitude: 33.1,
                                    geohash: "abcdefg",
                                    desc: "test-beacon-1")
        let beaconData2 = BeaconData(id: 3,
                                     regionId: 1,
                                     beaconType: "Normal",
                                     uuid: uuid,
                                     major: 1,
                                     minor: 3,
                                     indoor: true,
                                     movable: false,
                                     txPower: -12,
                                     latitude: 127.1,
                                     longitude: 33.1,
                                     geohash: "abcdefg",
                                     desc: "test-beacon-2")
        let beaconData3 = BeaconData(id: 2,
                                     regionId: 1,
                                     beaconType: "Normal",
                                     uuid: uuid,
                                     major: 1,
                                     minor: 2,
                                     indoor: false,
                                     movable: false,
                                     txPower: -12,
                                     latitude: 127.1,
                                     longitude: 33.1,
                                     geohash: "abcdefg",
                                     desc: "test-beacon-3")

        return TamraNearbySpots(spots: [
                TamraSpot(beaconData: beaconData1, region: region),
                TamraSpot(beaconData: beaconData1, region: region),
                TamraSpot(beaconData: beaconData2, region: region),
                TamraSpot(beaconData: beaconData3, region: region)
        ])
    }

    override func spec() {
        describe("TamraNearbySpots") {
            it("필터 기능을 제공한다.") {
                // when
                let filteredSpots = self.testSpots.filter {
                    spot in
                    return spot.indoor == true
                }

                // then
                expect(filteredSpots.size).to(equal(3))
            }

            it("정렬 기능을 제공한다.") {
                // when
                let orderedSpots = self.testSpots.orderBy {
                    spot1, spot2 in
                    return spot1.id < spot2.id
                }

                // then
                var prevId = 0
                for spot in orderedSpots {
                    expect(prevId <= spot.id).to(beTrue())
                    prevId = spot.id
                }
            }

            it("특정 조건을 모두 만족하는지 확인한다.") {
                // when & then
                expect(self.testSpots.all { $0.movable == false }).to(beTrue())
                expect(self.testSpots.all { $0.indoor == true }).to(beFalse())
            }

            it("특정 조건을 하나라도 만족하는지 확인한다.") {
                // when & then
                expect(self.testSpots.any { $0.movable == true }).to(beFalse())
                expect(self.testSpots.any { $0.indoor == false }).to(beTrue())
            }

            it("특정 값으로 변환한다.") {
                // when
                let ids = self.testSpots.transform { $0.id }

                // then
                expect(ids).to(equal([1, 1, 3, 2]))
            }

            it("특정 키값으로 dictionary를 만든다.") {
                // when
                let dict = self.testSpots.index { $0.indoor }

                // then
                expect(dict[true]!.count).to(equal(3))
                expect(dict[false]!.count).to(equal(1))
            }

            it("TamraSpot을 키로 dictionary를 만든다.") {
                // when
                let dict = self.testSpots.toMap { $0.id }

                // then
                for (key, value) in dict {
                    expect(value).to(equal(key.id))
                }
            }
        }
    }
}