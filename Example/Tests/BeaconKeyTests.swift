import Quick
import Nimble
@testable import Tamra

class BeaconKeySpec: QuickSpec {
    override func spec() {
        describe("BeaconKey") {
            it("사전의 키로 사용될 수 있다.") {
                // given
                let key1 = BeaconKey(uuid: NSUUID(UUIDString: "d7efc29e-d6f1-47bd-a7f7-da9bd95f1d06")!, major: 0, minor: 0)
                let key2 = BeaconKey(uuid: NSUUID(UUIDString: "dc8f3cc7-8ffb-420c-bcaf-9eaa48623cec")!, major: 0, minor: 0)
                let key3 = BeaconKey(uuid: NSUUID(UUIDString: "d7efc29e-d6f1-47bd-a7f7-da9bd95f1d06")!, major: 1, minor: 0)
                let key4 = BeaconKey(uuid: NSUUID(UUIDString: "d7efc29e-d6f1-47bd-a7f7-da9bd95f1d06")!, major: 0, minor: 1)

                let dict = [
                        key1: "key1",
                        key2: "key2",
                        key3: "key3",
                        key4: "key4"
                ]

                // when & then
                expect(dict[key1]).to(equal("key1"))
                expect(dict[key2]).to(equal("key2"))
                expect(dict[key3]).to(equal("key3"))
                expect(dict[key4]).to(equal("key4"))
            }

            it("동등 비교를 지원한다.") {
                // given
                let one = BeaconKey(uuid: NSUUID(UUIDString: "d7efc29e-d6f1-47bd-a7f7-da9bd95f1d06")!, major: 0, minor: 0)
                let same = BeaconKey(uuid: NSUUID(UUIDString: "d7efc29e-d6f1-47bd-a7f7-da9bd95f1d06")!, major: 0, minor: 0)
                let notSame = BeaconKey(uuid: NSUUID(UUIDString: "d7efc29e-d6f1-47bd-a7f7-da9bd95f1d06")!, major: 0, minor: 1)

                // when & then
                expect(one == same).to(equal(true))
                expect(one == notSame).to(equal(false))
            }
        }
    }
}
