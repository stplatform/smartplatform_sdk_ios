import Quick
import Nimble
@testable import Tamra

class RangedBeaconSpec: QuickSpec {
    override func spec() {
        describe("RangedBeacon") {
            it("accuracy 값에 따라 proximity 값이 올바르게 설정되어야 한다.") {
                // given
                let minOnImmediate = 0.0
                let maxOnImmediate = 1.0
                let minOnNear = 1.1
                let maxOnNear = 3.0
                let minOnFar = 3.1
                let maxOnFar = 10.0
                let under = -1.0
                let over = 10.1

                // when & then
                expect(RangedBeacon.testing(accuracy: under).proximity).to(equal("Unknown"))
                expect(RangedBeacon.testing(accuracy: minOnImmediate).proximity).to(equal("Immediate"))
                expect(RangedBeacon.testing(accuracy: maxOnImmediate).proximity).to(equal("Immediate"))
                expect(RangedBeacon.testing(accuracy: minOnNear).proximity).to(equal("Near"))
                expect(RangedBeacon.testing(accuracy: maxOnNear).proximity).to(equal("Near"))
                expect(RangedBeacon.testing(accuracy: minOnFar).proximity).to(equal("Far"))
                expect(RangedBeacon.testing(accuracy: maxOnFar).proximity).to(equal("Far"))
                expect(RangedBeacon.testing(accuracy: over).proximity).to(equal("Unknown"))
            }

            it("clone을 하면 다른 object가 나와야 한다") {
                // given
                let rangedBeacon = RangedBeacon.testing()

                // when & then
                expect(rangedBeacon == rangedBeacon.clone()).to(beFalse())
            }
        }
    }
}