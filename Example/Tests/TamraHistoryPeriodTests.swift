import Quick
import Nimble
@testable import Tamra

class TamraHistoryPeriodSpec: QuickSpec {
    override func spec() {
        describe("TamraHistoryPeriod") {
            it("현재부터 특정 초 이전의 기간을 생성할 수 있다.") {
                // given
                let seconds = 10

                // when
                let period = TamraHistoryPeriod(lastSeconds: seconds)

                // then
                expect(Int(period.to.timeIntervalSinceDate(period.from))).to(equal(seconds))
            }

            it("현재부터 특정 분 이전의 기간을 생성할 수 있다.") {
                // given
                let minutes = 10

                // when
                let period = TamraHistoryPeriod(lastMinutes: minutes)

                // then
                expect(Int(period.to.timeIntervalSinceDate(period.from))).to(equal(minutes * 60))

            }

            it("현재부터 특정 시간 이전의 기간을 생성할 수 있다.") {
                // given
                let hours = 10

                // when
                let period = TamraHistoryPeriod(lastHours: hours)

                // then
                expect(Int(period.to.timeIntervalSinceDate(period.from))).to(equal(hours * 60 * 60))

            }
        }
    }
}