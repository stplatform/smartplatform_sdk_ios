import Quick
import Nimble
import CoreLocation
@testable import Tamra

class CoreLocationExtentionsSpec: QuickSpec {
    override func spec() {
        describe("CLProximity") {
            it("문자열로 변경할 수 있다.") {
                // when & then
                expect(CLProximity.Immediate.stringValue).to(equal("Immediate"))
                expect(CLProximity.Near.stringValue).to(equal("Near"))
                expect(CLProximity.Far.stringValue).to(equal("Far"))
                expect(CLProximity.Unknown.stringValue).to(equal("Unknown"))
            }
        }

        describe("CLBeaconRegion") {
            it("TamraRegion 으로부터 새로운 객체를 생성할 수 있다.") {
                // given
                let regionData: RegionData = RegionData(
                        id: 1,
                        name: "test region",
                        regionType: "Normal",
                        uuid: NSUUID(),
                        latitude: 0,
                        longitude: 0,
                        geohash: "",
                        radius: 0,
                        desc: "this is test region")
                let tamraRegion = TamraRegion(regionData: regionData)

                // when
                let beaconRegion: CLBeaconRegion! = CLBeaconRegion(for: tamraRegion)

                // then
                expect(beaconRegion.proximityUUID).to(equal(regionData.uuid))
            }
        }
    }
}