import Quick
import Nimble
@testable import Tamra

class TestSupports {

}

extension RangedBeacon {
    static func testing(accuracy accuracy: Double = -1.0) -> RangedBeacon {
        let region = Region(uuid: NSUUID(), identifier: "test")
        return RangedBeacon(region: region, major: 0, minor: 0, accuracy: accuracy, rangedAt: NSDate())
    }
}