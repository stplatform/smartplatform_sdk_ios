import Quick
import Nimble
@testable import Tamra

class BeaconManagerSpec: QuickSpec {
    override func spec() {
        describe("BeaconManager") {
            it("모니터링을 시작하면 모니터링 지역 목록에 추가되고 모니터링을 종료하면 지역 목록에서 제거되어야 한다.") {
                // given
                let beaconManager = BeaconManager()
                let uuid = NSUUID()
                let identifier = "test"

                // when
                beaconManager.startMonitoring(uuid, identifier: identifier)

                // then
                expect(beaconManager.isOnMonitor(uuid)).to(beTrue())

                // when
                beaconManager.stopMonitoring(uuid, identifier: identifier)

                // then
                expect(beaconManager.isOnMonitor(uuid)).to(beFalse())
            }

            it("모니터링이 시작되지 않았으면 주변 비콘 정보를 보내지 말아야 한다.") {
                // given
                let beaconManager = BeaconManager()
                let uuid = NSUUID()
                let region = Region(uuid: uuid, identifier: "test")
                let beacons = [
                        Beacon(uuid: uuid, major: 0, minor: 0, accuracy: 0)
                ]

                class Delegate: NSObject, BeaconManagerDelegate {
                    var ranged: Bool = false

                    @objc func beaconManager(didRangeBeacons beacons: [Beacon], inRegion region: Region) {
                        ranged = true
                    }
                }

                let delegate = Delegate()

                // when
                beaconManager.delegate = delegate
                beaconManager.notifyRangedBeacons(didRangeBeacons: beacons, inRegion: region)

                // then
                expect(delegate.ranged).to(beFalse())
            }
        }
    }
}
