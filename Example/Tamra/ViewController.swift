import UIKit
import Tamra

class ViewController: UIViewController, TamraManagerDelegate {

    let tamraManager = TamraManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        Tamra.requestLocationAuthorization()
        tamraManager.delegate = self
        tamraManager.ready()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func tamraManagerDelegate(didReadyState state: TamraManagerState) {
        if state == .Ignored {
            print("Tamra Manager Loading Skipped.")
        }

        if state != .LoadingFailed {
            print("Tamra Manager Start Monitoring.")
//            tamraManager.startMonitoring(forId: 5)
//            tamraManager.startMonitoring()
        } else {
            print("Tamra Manager Loading Failed.")
        }
    }

    func tamraManager(didEnterRegion region: TamraRegion) {
        print("did enter region : \(region.id) - \(region.name!)")
    }

    func tamraManager(didExitRegion region: TamraRegion) {
        print("did exit region : \(region.id) - \(region.name!)")
    }

    func tamraManager(didRangeSpots spots: TamraNearbySpots) {
        print("did range spots : \(spots.size)")
        for spot in spots {
            print("spot : \(spot.regionName) -> \(spot.desc), \(spot.proximity), \(spot.accuracy)")
            Tamra.logHistory(spot)
            for history in Tamra.histories(5) {
                print("history: \(history.description) -> \(history.issuedAt)")
            }
        }
    }

}
