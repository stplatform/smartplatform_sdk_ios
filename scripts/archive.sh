#!/bin/sh
echo "[ARCHIVE START] $(date +'%Y-%m-%d %H:%M:%S')"

BASEDIR=$(cd "$(dirname $0)/.." && pwd)
echo "[BASE DIRECTORY] $BASEDIR"

echo "[ARCHIVE..]"
xcodebuild -workspace "${BASEDIR}/Tamra.xcworkspace" -scheme Tamra -destination generic/platform=iOS archive > "${BASEDIR}/output/archive.log" 2>&1

ZIPFILE="${BASEDIR}/deploy/Tamra.zip"
echo "[CLEAR ZIP FILE PREVIOUSLY..]"
rm -f "${ZIPFILE}"

cd "${BASEDIR}/output"
echo "[FRAMEWORK INFO]"
file Tamra.framework/Tamra

echo "[ZIP FRAMEWORK..]"
zip -r "${ZIPFILE}" Tamra.framework

echo "[ZIP FILE] ${ZIPFILE}"

echo "[ARCHIVE FINISH] $(date +'%Y-%m-%d %H:%M:%S')"