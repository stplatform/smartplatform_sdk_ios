Pod::Spec.new do |s|
	s.name = 'Tamra'
        s.version = '0.3.0'
	s.summary = 'iOS Beacon SDK for Smart Tourism Platform'
        s.homepage = 'http://220.124.222.194'
	s.author = { 'Name' => 'cocoon.tf@kakaocorp.com' }
	s.license = { :type => 'Copyright', :text => 'Copyright 2016 Kakao Corp. All Rights Reserved.' }
	s.platform = :ios
        s.source = { :http => 'https://bitbucket.org/stplatform/smartplatform_sdk_ios/raw/2c17b3ec4a1cc09faea83d36f95601294c2459e8/deploy/Tamra.zip' }
	s.ios.deployment_target = '10.3'
	s.ios.vendored_frameworks = 'Tamra.framework'
	s.dependency 'ReachabilitySwift', '3.0'
	s.dependency 'Alamofire', '4.7.2'
	s.dependency 'RealmSwift', '3.7.4'
	s.dependency 'CryptoSwift', '0.9.0'
end
